package cyberattacktestngpackage;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

@SuppressWarnings("unused")
public class CyberAttackStatsTest {

	public static WebDriver driver = null;

	@BeforeSuite
	public void beforeSuite() {

		driver = CommonFunctions.launchURL();

	}

	@Test(priority = 1)
	public void verifyCyberAttackDefaultData() throws InterruptedException {

		HashMap<String, ArrayList<String>> actualTableMap = CommonFunctions.getWebTable(driver);
		HashMap<String, ArrayList<String>> expectedTableMap = CommonFunctions.getExcelData(driver, "Master");

		AssertJUnit.assertTrue(actualTableMap.equals(expectedTableMap));

	}

	@Test(priority = 2)
	public void verifySortByNameWithoutFilter() throws InterruptedException {

		// Functional check starts from here

		CommonFunctions.sortWebTable(driver, "Name");

		HashMap<String, ArrayList<String>> actualTableMap = CommonFunctions.getWebTable(driver);
		HashMap<String, ArrayList<String>> expectedTableMap = CommonFunctions.getExcelData(driver, "Master");

		AssertJUnit.assertTrue(actualTableMap.equals(expectedTableMap));

	}

	@Test(priority = 3)
	public void verifySortByNumberOfCasesWithoutFilter() throws InterruptedException {

		// Functional check starts from here

		CommonFunctions.sortWebTable(driver, "Number of cases");

		HashMap<String, ArrayList<String>> actualTableMap = CommonFunctions.getWebTable(driver);
		HashMap<String, ArrayList<String>> expectedTableMap = CommonFunctions.getExcelData(driver, "Master");

		AssertJUnit.assertTrue(actualTableMap.equals(expectedTableMap));

	}

	@Test(priority = 4)
	public void verifySortByImpactScoreWithoutFilter() throws InterruptedException {

		// Functional check starts from here

		CommonFunctions.sortWebTable(driver, "Impact score");

		HashMap<String, ArrayList<String>> actualTableMap = CommonFunctions.getWebTable(driver);
		HashMap<String, ArrayList<String>> expectedTableMap = CommonFunctions.getExcelData(driver, "Master");

		AssertJUnit.assertTrue(actualTableMap.equals(expectedTableMap));

	}

	@Test(priority = 5)
	public void verifySortByComplexityWithoutFilter() throws InterruptedException {

		// Functional check starts from here

		CommonFunctions.sortWebTable(driver, "Complexity");

		HashMap<String, ArrayList<String>> actualTableMap = CommonFunctions.getWebTable(driver);
		HashMap<String, ArrayList<String>> expectedTableMap = CommonFunctions.getExcelData(driver, "Master");

		AssertJUnit.assertTrue(actualTableMap.equals(expectedTableMap));

	}

	@Test(priority = 6)
	public void verifySortByNameWithFilter_aTozLowerCase() throws InterruptedException {

		// Functional check starts from here

		CommonFunctions.sortWebTable(driver, "Name");

		// Filter Code for a to z

		char[] filterdata = new char[26];
		for (int k = 0; k < 26; k++) {
			filterdata[k] = (char) (97 + k);
			// System.out.println("alphabets :"+ filterdata[k]);

			String filterString = Character.toString(filterdata[k]);

			WebElement filterDataElement = driver.findElement(By.id("filter-input"));
			filterDataElement.clear();
			filterDataElement.sendKeys(filterString);

			// Thread.sleep(3000);

			// Filter Code

			HashMap<String, ArrayList<String>> actualTableMap = CommonFunctions.getWebTable(driver);
			HashMap<String, ArrayList<String>> expectedTableMap = CommonFunctions.getExcelData(driver, filterString);

			AssertJUnit.assertTrue(actualTableMap.equals(expectedTableMap));

			CommonFunctions.clearFilterBox(driver);

		}

	}

	@Test(priority = 7)
	public void verifySortByNumberOfCasesWithFilteraTozLowerCase() throws InterruptedException {

		// Functional check starts from here

		CommonFunctions.sortWebTable(driver, "Number of cases");

		// Filter Code for a to z

		char[] filterdata = new char[26];
		for (int k = 0; k < 26; k++) {
			filterdata[k] = (char) (97 + k);
			// System.out.println("alphabets :"+ filterdata[k]);

			String filterString = Character.toString(filterdata[k]);

			WebElement filterDataElement = driver.findElement(By.id("filter-input"));
			filterDataElement.clear();
			filterDataElement.sendKeys(filterString);

			// Thread.sleep(3000);

			// Filter Code

			HashMap<String, ArrayList<String>> actualTableMap = CommonFunctions.getWebTable(driver);
			HashMap<String, ArrayList<String>> expectedTableMap = CommonFunctions.getExcelData(driver, filterString);

			AssertJUnit.assertTrue(actualTableMap.equals(expectedTableMap));

			CommonFunctions.clearFilterBox(driver);

		}

	}

	@Test(priority = 8)
	public void verifySortByImpactScoreWithFilteraTozLowerCase() throws InterruptedException {

		// Functional check starts from here

		CommonFunctions.sortWebTable(driver, "Impact score");

		// Filter Code for a to z

		char[] filterdata = new char[26];
		for (int k = 0; k < 26; k++) {
			filterdata[k] = (char) (97 + k);
			// System.out.println("alphabets :"+ filterdata[k]);

			String filterString = Character.toString(filterdata[k]);

			WebElement filterDataElement = driver.findElement(By.id("filter-input"));
			filterDataElement.clear();
			filterDataElement.sendKeys(filterString);

			// Thread.sleep(3000);

			// Filter Code

			HashMap<String, ArrayList<String>> actualTableMap = CommonFunctions.getWebTable(driver);
			HashMap<String, ArrayList<String>> expectedTableMap = CommonFunctions.getExcelData(driver, filterString);

			AssertJUnit.assertTrue(actualTableMap.equals(expectedTableMap));

			CommonFunctions.clearFilterBox(driver);

		}

	}

	@Test(priority = 9)
	public void verifySortByComplexityWithFilteraTozLowerCase() throws InterruptedException {

		// Functional check starts from here

		CommonFunctions.sortWebTable(driver, "Complexity");

		// Filter Code for a to z

		char[] filterdata = new char[26];
		for (int k = 0; k < 26; k++) {
			filterdata[k] = (char) (97 + k);
			// System.out.println("alphabets :"+ filterdata[k]);

			String filterString = Character.toString(filterdata[k]);

			WebElement filterDataElement = driver.findElement(By.id("filter-input"));
			filterDataElement.clear();
			filterDataElement.sendKeys(filterString);

			// Thread.sleep(3000);

			// Filter Code

			HashMap<String, ArrayList<String>> actualTableMap = CommonFunctions.getWebTable(driver);
			HashMap<String, ArrayList<String>> expectedTableMap = CommonFunctions.getExcelData(driver, filterString);

			AssertJUnit.assertTrue(actualTableMap.equals(expectedTableMap));

			CommonFunctions.clearFilterBox(driver);

		}

	}

	@Test(priority = 10)
	public void verifySortByNameWithFilter_AtoZUpperCase() throws InterruptedException {

		// Functional check starts from here

		CommonFunctions.sortWebTable(driver, "Name");

		// Filter Code for a to z

		char[] filterdata = new char[26];
		for (int k = 0; k < 26; k++) {
			filterdata[k] = (char) (65 + k);
			// System.out.println("alphabets :"+ filterdata[k]);

			String filterString = Character.toString(filterdata[k]);

			WebElement filterDataElement = driver.findElement(By.id("filter-input"));
			filterDataElement.clear();
			filterDataElement.sendKeys(filterString);

			// Thread.sleep(3000);

			// Filter Code

			HashMap<String, ArrayList<String>> actualTableMap = CommonFunctions.getWebTable(driver);
			HashMap<String, ArrayList<String>> expectedTableMap = CommonFunctions.getExcelData(driver,
					filterString.toLowerCase().toString());

			AssertJUnit.assertTrue(actualTableMap.equals(expectedTableMap));

			CommonFunctions.clearFilterBox(driver);

		}

	}

	@Test(priority = 11)
	public void verifySortByNumberOfCasesWithFilterAtoZUpperCase() throws InterruptedException {

		// Functional check starts from here

		CommonFunctions.sortWebTable(driver, "Number of cases");

		// Filter Code for a to z

		char[] filterdata = new char[26];
		for (int k = 0; k < 26; k++) {
			filterdata[k] = (char) (65 + k);
			// System.out.println("alphabets :"+ filterdata[k]);

			String filterString = Character.toString(filterdata[k]);

			WebElement filterDataElement = driver.findElement(By.id("filter-input"));
			filterDataElement.clear();
			filterDataElement.sendKeys(filterString);

			// Thread.sleep(3000);

			// Filter Code

			HashMap<String, ArrayList<String>> actualTableMap = CommonFunctions.getWebTable(driver);
			HashMap<String, ArrayList<String>> expectedTableMap = CommonFunctions.getExcelData(driver,
					filterString.toLowerCase().toString());

			AssertJUnit.assertTrue(actualTableMap.equals(expectedTableMap));

			CommonFunctions.clearFilterBox(driver);

		}

	}

	@Test(priority = 12)
	public void verifySortByImpactScoreWithFilterAtoZUpperCase() throws InterruptedException {

		// Functional check starts from here

		CommonFunctions.sortWebTable(driver, "Impact score");

		// Filter Code for a to z

		char[] filterdata = new char[26];
		for (int k = 0; k < 26; k++) {
			filterdata[k] = (char) (65 + k);
			// System.out.println("alphabets :"+ filterdata[k]);

			String filterString = Character.toString(filterdata[k]);

			WebElement filterDataElement = driver.findElement(By.id("filter-input"));
			filterDataElement.clear();
			filterDataElement.sendKeys(filterString);

			// Thread.sleep(3000);

			// Filter Code

			HashMap<String, ArrayList<String>> actualTableMap = CommonFunctions.getWebTable(driver);
			HashMap<String, ArrayList<String>> expectedTableMap = CommonFunctions.getExcelData(driver,
					filterString.toLowerCase().toString());

			AssertJUnit.assertTrue(actualTableMap.equals(expectedTableMap));

			CommonFunctions.clearFilterBox(driver);

		}

	}

	@Test(priority = 13)
	public void verifySortByComplexityWithFilterAtoZUpperCase() throws InterruptedException {

		// Functional check starts from here

		CommonFunctions.sortWebTable(driver, "Complexity");

		// Filter Code for a to z

		char[] filterdata = new char[26];
		for (int k = 0; k < 26; k++) {
			filterdata[k] = (char) (65 + k);
			// System.out.println("alphabets :"+ filterdata[k]);

			String filterString = Character.toString(filterdata[k]);

			WebElement filterDataElement = driver.findElement(By.id("filter-input"));
			filterDataElement.clear();
			filterDataElement.sendKeys(filterString);

			// Thread.sleep(3000);

			// Filter Code

			HashMap<String, ArrayList<String>> actualTableMap = CommonFunctions.getWebTable(driver);
			HashMap<String, ArrayList<String>> expectedTableMap = CommonFunctions.getExcelData(driver,
					filterString.toLowerCase().toString());

			AssertJUnit.assertTrue(actualTableMap.equals(expectedTableMap));

			CommonFunctions.clearFilterBox(driver);

		}

	}

	@AfterSuite
	public void afterSuite() {
		// close Fire fox
		driver.close();
		System.exit(0);
	}

}
