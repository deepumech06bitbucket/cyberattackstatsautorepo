package cyberattacktestngpackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.AssertJUnit;

public class CommonFunctions {

	public static WebDriver launchURL() {

		String path = System.getProperty("user.dir");
		// System.out.println(path);
		System.setProperty("webdriver.chrome.driver", path + "\\resources\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		String actualTitle = "";
		String actualPageName = "";

		// launch Fire fox and direct it to the Base URL
		driver.get(Constant.URL);

		// get the actual value of the title
		actualTitle = driver.getTitle();
		// System.out.println(actualTitle);

		// get the page
		actualPageName = driver.findElement(By.xpath("//*[@id=\"app\"]/h1")).getText();

		// compare the actual title of the page with the expected one and print the
		// result as "Passed" or "Failed"
		if (actualTitle.contentEquals(Constant.expectedTitle)) {
			// System.out.println("Test Passed! - Page Title is:"+ actualTitle );
		} else {
			// System.out.println("Test Failed - Page Title is:"+ actualTitle);
		}

		// Validate the page Title
		AssertJUnit.assertEquals(Constant.expectedTitle, actualTitle);

		// Validate the page Name

		AssertJUnit.assertEquals(Constant.expectedPageName, actualPageName);

		return driver;
	}

	public static HashMap<String, ArrayList<String>> getWebTable(WebDriver driver) {
		// Start of Web Table collections

		List<WebElement> tableData = driver.findElements(By.xpath("//div[@class='table-row']"));
		HashMap<String, ArrayList<String>> actualTableMap = new HashMap<String, ArrayList<String>>();

		// System.out.println("Data Table Size: "+ tableData.size());

		for (int i = 1; i <= tableData.size(); i++) {

			String nameXpath = "//div[@class='table-row'][" + i + "]/div[contains(@class,'data-name')][1]";
			String name = driver.findElement(By.xpath(nameXpath)).getText();
			String casesXpath = "//div[@class='table-row'][" + i + "]/div[contains(@class,'data-cases')][1]";
			String cases = driver.findElement(By.xpath(casesXpath)).getText();
			String averageImpactXpath = "//div[@class='table-row'][" + i
					+ "]/div[contains(@class,'data-averageImpact')][1]";
			String averageImpact = driver.findElement(By.xpath(averageImpactXpath)).getText();
			String complexityXpath = "//div[@class='table-row'][" + i + "]/div[contains(@class,'data-complexity')][1]";
			String complexity = driver.findElement(By.xpath(complexityXpath)).getText();

			ArrayList<String> rowValList = new ArrayList<String>();
			rowValList.add(cases);
			rowValList.add(averageImpact);
			rowValList.add(complexity);
			actualTableMap.put(name, rowValList);
			// System.out.println(name + ":" + cases + ":" + averageImpact + ":" +
			// complexity);
		}

		// End of Web Table collections

		return actualTableMap;
	}

	public static HashMap<String, ArrayList<String>> getExcelData(WebDriver driver, String sheetName) {
		// Start of Excel collections

		HashMap<String, ArrayList<String>> expectedTableMap = new HashMap<String, ArrayList<String>>();

		// Create an object of File class to open xlsx file
		File file = new File(Constant.Path_TestData);

		// Create an object of FileInputStream class to read excel file
		FileInputStream inputStream = null;
		try {
			inputStream = new FileInputStream(file);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Creating workbook instance that refers to .xls file
		XSSFWorkbook wb = null;
		try {
			wb = new XSSFWorkbook(inputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Creating a Sheet object using the sheet Name
		XSSFSheet sheet = wb.getSheet(sheetName);

		// get all rows in the sheet
		int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();

		// System.out.println("RowCount :"+ rowCount);

		// iterate over all the row to print the data present in each cell.
		for (int i = 1; i <= rowCount; i++) {

			// get cell count in a row
			int cellcount = sheet.getRow(i).getLastCellNum();

			// iterate over each cell to print its value
			// System.out.println("Row"+ i+" data is :");

			ArrayList<String> rowValList = new ArrayList<String>();
			String name = null;
			for (int j = 0; j < cellcount; j++) {

				if (j == 0) {
					name = sheet.getRow(i).getCell(j).getStringCellValue();
				} else {
					rowValList.add(sheet.getRow(i).getCell(j).getStringCellValue());
				}

				expectedTableMap.put(name, rowValList);

			}
		}

		return expectedTableMap;
	}
	
	
	public static void clearFilterBox(WebDriver driver) {
		WebElement filterDataElementClear = driver.findElement(By.id("filter-input"));
		filterDataElementClear.sendKeys(Keys.BACK_SPACE);
	}
	
	
	public static void sortWebTable(WebDriver driver, String sortText) {

		  Select SortData = new Select(driver.findElement(By.name("sort-select")));
	      SortData.selectByVisibleText(sortText); 
	}

}
