package cyberattacktestngpackage;

public class Constant {
	
	public static final String URL = "https://mystifying-beaver-ee03b5.netlify.app/";
	
	public static final String expectedTitle = "simplesite";
	
	public static final String expectedPageName = "Cyber attack completely fake statistics";

	public static final String Path_TestData = "C:\\Eclipse Setup\\Eclipse_JavaPractice\\CyberAttack\\resources\\CyberAttack.xlsx";

}
